/**
 * Created by Alexis on 20/04/2017.
 */

function lightREST(room, value, action, selection) {
    var data = JSON.stringify({"type":"light","room":room, "value":value, "action":action,"selection": selection});
    serverRequest(data, function(){});
}

function movieMode() {
    lightREST(5, 25, 'bri', 'light')
    lightREST(10, 50, 'bri', 'light')
}

function briMin(group){
    lightREST(group, 25, 'bri', 'group')
    // refreshBriFor(group);
}

function briMax(group){
    lightREST(group, 254, 'bri', 'group')
    // refreshBriFor(group);
}

function patriote(){
    lightREST(arche1, [0.06666666667,0.1555555556], 'xy', 'light');
    lightREST(arche2, [0.3333333333333333,0.3333333333333333], 'xy', 'light');
    lightREST(arche3, [0.7308868502,0.1314984709], 'xy', 'light');
}