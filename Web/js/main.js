/**
 * Created by Alexis on 20/04/2017.
 */
var timerStart;
var duree;
var timeoutID;
var url;
var timeout;
var local;
var key = "91BXD01V7bvy0VTrX4Hy5T2ZSI0p2WSzOywGKZrCDUploLvJBRWzZqFlOMuEDgPX";

$(document).ready(function(){
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal').modal();
    $('.materialboxed').materialbox();
    $(".button-collapse").sideNav();

    url = "http://192.168.1.40:8090";
    //url = "http://fourrier-1704.homedns.org:9080";//TODO remove
    timeout = 2000;
    local = true;

    //TODO regarder etat alarme et afficher
    var data = JSON.stringify({"type":"general", "local": local});
    serverRequest(data, function () {
        console.log(xhr);
        if (xhr.readyState == 4 && xhr.status == 200) {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            // temp = json["temperature"];
            if(json["alarm"]==='true'){
                alarmOn();
            }
            else {
                alarmOff();
            }
            var temp = json["temperature"];

            //Ajout des elements graphiques
            lightAndSonos("#room_cuisine", "Cuisine", "Cuisine", "Cuisine", temp["cuisine"]);
            lightAndSonos("#room_salon", "Salon", "Salon", "Salon", temp["salon"]);
            lightAndSonos("#room_bar", "Bar", "Bar", "Salon", temp["bar"]);
            //sonosOnly("#room_sdb", "Salle de bain", "Salle de bain");

            lightOnly("#room_arche", "Arche", "Arche");
            twoLights("#room_ext", ["Exterieur", "lampadaire"],"Ext&eacute;rieur", ["Maison","Lampadaire"], temp["exterieur"]);
            //sonosOnly("#room_piscine", "Piscine", "Piscine")

            goTop('fast');
        }
    });


    timerStart = false;
    duree = 20;

    //Clavier alarme
    initialiser_calc('calc');

});

function sonosOnly(id, room, sonos_name, temperature) {
    var room_name = "<div class='h5_room'>"+room+"</div>";
    var room_temp = "";
    if(temperature!=undefined){
        room_temp = "<div class='affichage_temp'>"+temperature+"°c</div>";
    }
    $( id).append( room_name+room_temp);
    addSonosToRoom(id, sonos_name);
}

function lightOnly(id, hueName,room, temperature) {
    var room_name = "<div class='h5_room'>"+room+"</div>";
    var room_temp = "";
    if(temperature!=undefined){
        room_temp = "<div class='affichage_temp'>"+temperature+"°c</div>";
    }
    $( id).append( room_name+room_temp);
    addLightToRoom(id, hueName);
    addRoomSubButton(id, hueName);

}

function twoLights(id, hueName, room, values, temperature) {
    var room_name = "<div class='h5_room'>"+room+"</div>";
    var room_temp = "";
    if(temperature!=undefined){
        room_temp = "<div class='affichage_temp'>"+temperature+"°c</div>";
    }
    $( id).append( room_name+room_temp);
    addLightToRoom(id, hueName[0], values[0]);
    $( id).append( "<br>");
    addLightToRoom(id, hueName[1], values[1]);
}

function lightAndSonos(id, room, hueName, sonos_name, temperature) {
    var room_name = "<div class='h5_room'>"+room+"</div>";
    var room_temp = "";
    if(temperature!=undefined){
        room_temp = "<div class='affichage_temp'>"+temperature+"°c</div>";
    }
    $( id).append( room_name+room_temp);
    addLightToRoom(id, hueName);
    addRoomSubButton(id, hueName);
    $( id).append( "<br>");
    //addSonosToRoom(id, sonos_name);
}

function addLightToRoom(id, hueName, text){
    if(text===undefined){
        text='Lumi&egrave;re'
    }
    var main = "<div class='horizontal click-to-toggle'><a class='btn-floating waves-effect waves-light amber card-fab' onclick='lightREST(\""+hueName+"\", \"inverse\", \"on\", \"group\")'><i class='material-icons'>wb_incandescent</i></a>";
    main = '<a class="waves-effect waves-light btn amber button_room" onclick="lightREST(\''+hueName+'\', \'inverse\', \'on\', \'group\')"><i class="material-icons left">wb_incandescent</i>'+text+'</a>'
    $( id).append(main);

};

function addRoomSubButton(id, room) {
    if(room==="Salon"){
        addMinMaxThemeLightSubButton(id, room, 'movieMode()', 'theaters', 'style="font-size: 30px"');
    }
    else if(room==="Arche"){
        addMinMaxThemeLightSubButton(id, room, 'movieMode()', 'autorenew', 'style="font-size: 30px"');
    }
    else {
        addMinMaxLightSubButton(id, room);
    }
}

function addMinMaxLightSubButton(id, hueName) {
    addLightSubButton(id, ['briMin(\''+hueName+'\')', 'briMax(\''+hueName+'\')'], ["wb_sunny", "wb_sunny"], ['style="font-size: 20px"','style="font-size: 30px"'])
}

function addMinMaxThemeLightSubButton(id, hueName, fonction, icone, stl) {
    addLightSubButton(id, ['briMin(\''+hueName+'\')', 'briMax(\''+hueName+'\')', fonction], ["wb_sunny", "wb_sunny", icone], ['style="font-size: 20px"','style="font-size: 30px"', stl])
}

function addLightSubButton(id, fonctions, icones, style) {
    var a = '';
    for(var i=0;i<fonctions.length;i++){
        var c = "sub_button_room_";
        c+=fonctions.length;
        if(fonctions.length==2 && i==0){
            c+= "_left";
        }
        else if(fonctions.length==3 && i==1){
            c+= "_center";
        }
        a+='<a class="waves-effect waves-light btn amber button_room '+c+'" onclick="'+fonctions[i]+'"><i class="material-icons center" '+style[i]+'>'+icones[i]+'</i></a>';
    }
    $( id).append(a);
}

function addSonosToRoom(room, sonos_room){
    $(room).append( '<a class="waves-effect waves-light btn amber button_room" onclick="sonosPlayPause(\''+sonos_room+'\')"><i class="material-icons left">play_arrow</i>Sonos</a>');
};

function toto() {
    console.log("toto");
}

function openModal() {
    $('#modal1').modal('open');
}

function activerAlarme() {
    var data = JSON.stringify({"type":"alarm", "action":"activate"});
    serverRequest(data, function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var json = JSON.parse(xhr.responseText);
            if(json["succes"]==='true'){
                alarmOn();
            }
        }
    });
}

function activer20() {
    var data = JSON.stringify({"type":"alarm", "action":"activate20"});
    serverRequest(data, function(a){});
}

function deactiverAlarme() {
    var code = $("#calc_resultat").val();
    var data = JSON.stringify({"type":"alarm", "action":"disable", "code":+code});
    initialiser_calc('calc');

    serverRequest(data, function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            console.log(xhr.responseText)
            var json = JSON.parse(xhr.responseText);
            if(json["succes"]==='true'){
                alarmOff();
            }
            else{
                var $toastContent = $('<span>'+json['error']+'</span>');
                Materialize.toast($toastContent, 3000);
            }
        }
    });
}

function alarmOn() {
    $("#home").hide();
    $("#cam").hide();
    $("#alarme_off").hide();
    $("#alarme_on").show();
}

function alarmOff() {
    $("#home").show();
    $("#cam").show();
    $("#alarme_off").show();
    $("#alarme_on").hide();
}

function serverRequest(data, answerFunction) {
    // Sending and receiving data in JSON format using POST method
    //
    xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.onreadystatechange = answerFunction;
    xhr.timeout = timeout; // Set timeout to 4 seconds (4000 milliseconds)
    xhr.ontimeout = function () { //TODO
        if(local) {
            url = "http://fourrier-1704.homedns.org:9080";
            timeout = 30000;
            local = false;
            console.log("Time out !");
            serverRequest(data, answerFunction);
        }
    };
    xhr.send(data);
}

function getCave() {
    var data = JSON.stringify({"type":"settings", "action":"get"});//TODO
    serverRequest(data, function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var json = JSON.parse(xhr.responseText);
            console.log(json);//TODO remove
        }
    });
}

function timer(recurcif){
    var compteur=document.getElementById('compteur');
    s=duree;
    m=0;h=0;
    if(!recurcif){
        if(timerStart){
            timerStart = false;
            duree = 20;
            compteur.innerHTML="D&eacute;marrer";
            window.clearTimeout(timeoutID);
            return;
        }
        else {
            timerStart = true;
            activer20();
        }
    }

    if(s<0){
        console.log(s)
    }
    else if (s==0){
        activerAlarme();
        timer(false);//deactivation du timer et remise a zero
        return;
    }
    else{
        if(s>59){
            m=Math.floor(s/60);
            s=s-m*60
        }
        if(m>59){
            h=Math.floor(m/60);
            m=m-h*60
        }
        if(s<10){
            s="0"+s
        }
        if(m<10){
            m="0"+m
        }
        compteur.innerHTML=s;
    }
    duree-=1;
    timeoutID = window.setTimeout("timer(true);",999);
}

function goTop(speed) {
    $('html, body').animate({ scrollTop: 0 }, speed);
}

function scrollTo(id) {
    $("html, body").animate({ scrollTop: $(id).offset().top }, 700);
}